# -*- coding: utf-8 -*-
"""
Created on Sun Feb 25 10:16:21 2018

@author: DM390
"""
import re

def get_date_index(texts):
      #remove st and symbols from string
      pattern1 = '(\d{1,2}\s+)(Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)(\s+\d{4})'
      
      #remove symbol(',') from string
      pattern2 = '(Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)(\s+\d{1,2})(\s+\d{4})'
      
      #remove symbol ('.','/')
      pattern3 = '\d{2} \d{2} \d{4}'
      
      text_list = []
      for text_tag in texts[:100]:
            text = ' '.join([i for i in text_tag.itertext()])
            text = text.strip()
            text_list.append(text)
      
      
      index_list = []
      for ind,text in enumerate(text_list):
            temp_text = re.sub('[^a-zA-Z0-9 ]|\.', ' ', text)
            temp_text = temp_text.replace('st','')
            #temp_text = text.replace('st','').replace('-',' ')
            if re.search(pattern1,temp_text,re.IGNORECASE):
                  index_list.append(ind)
      
            #temp_text = text.replace(',',' ')
            elif re.search(pattern2,temp_text,re.IGNORECASE):
                  index_list.append(ind)
      
            #temp_text = text.replace('.',' ').replace('/',' ')
            elif re.search(pattern3,temp_text,re.IGNORECASE):
                  index_list.append(ind)
            
      return index_list[-1]



#xml_file = 'F:\Data_files2\SIL_ANNUAL-REPORT-2016-2017\splited_pages\BS_SIL_ANNUAL-REPORT-2016-2017_page_57.xml'
#xml_file = 'F:\Data_files2\SIL_ANNUAL-REPORT-2016-2017\splited_pages\PnL_SIL_ANNUAL-REPORT-2016-2017_page_58.xml'
#
#
#xml_file = r"F:\Untitled Folder\bs_pnlExtract\files\BS_NCC Annual Report2014_15_page_62.xml"
#xml_file = r"F:\Untitled Folder\bs_pnlExtract\files\PnL_Mangalore Chemicals 2017_page_49.xml"
#xml_file = r"F:\Untitled Folder\bs_pnlExtract\files\Bilt-Graphic-Paper-Annual-Report-2016-17_page_50.xml"
#
#with open(xml_file,'rb') as fd:
#     xml_string = fd.read()
#
#parser = etree.XMLParser(recover=True)
#root = etree.fromstring(xml_string, parser=parser)
#
##root = ET.fromstring(xml_string,parser)
#texts = root.findall('page/text')
#
#get_date_index(texts)
      