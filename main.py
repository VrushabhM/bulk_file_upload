# -*- coding: utf-8 -*-
"""
Created on Tue Apr 17 18:19:33 2018

@author: DM390
"""

#import SepFinPages

from SepFinPages import GetFinPages

import xml_BS_PnL3
import time
import pandas
from pandas import read_excel
import mark1,mark3
import clean_dataFrame
import os
import sys


def convert(splited_files):
    
    """Converts separarted pages to xml, extracts data from xml
       and save it in xlsx file
       
       Parametes:
       splited_files (list): list of splited files
       
       Return:
       converted_files (list): list of xlsx files 
    """
    
    converted_files = []
    for splited_file in splited_files:
        print('Extracting....')
        xml_file = xml_BS_PnL3.to_xml(splited_file);time.sleep(0.3)  #converts pdf to xml 
        xfile_path,_ = xml_BS_PnL3.get_xml_data(xml_file)            #returns Excel file path and Dataframe
        #print(xfile_path)
        converted_files.append(xfile_path)
        print("saved "+str(xfile_path))
    
    return converted_files


def save_excel(df_pdfTab,writter,sheet_name):
    temp_df = mark1.split_cells(df_pdfTab)
    table = clean_dataFrame.get_table(temp_df)
    table.to_excel(writter,sheet_name=sheet_name)
    #writter.save()


def structuredExcel(inputfile, converted_files,company_name,sep):
    """
    """
    result_file = inputfile.split('.pdf')[0]+sep+company_name+'.xlsx'
    writter = pandas.ExcelWriter(result_file)
    cnt = 1
    for xfile_path in converted_files:
        df_pdfTab = read_excel(xfile_path) # pandas method
        #last_index = df_pdfTab.index[-1]
        if xfile_path.split(sep)[-1].startswith('BS'):
            print("BalanceSheet")
            sheetName = 'BalanceSheet'+str(cnt)
            save_excel(df_pdfTab,writter,sheetName)
        elif xfile_path.split(sep)[-1].startswith('PnL'):
            print("ProfitAndLoss")
            sheetName = 'ProfitAndLoss'+str(cnt)
            save_excel(df_pdfTab,writter,sheetName)
        
        cnt += 1

    writter.save()


def main(inputfile,company_name):
    if inputfile.count('\\'):
        sep = '\\'
    else:
        sep = '/'

    obj = GetFinPages()
    obj.get_fin_pages(inputfile)

    os.chdir(obj.f_path)
    splited_files = obj.splited_page
    # print(time.time()-start_time)
    print('\n'.join(splited_files))
    converted_files = convert(obj.splited_page)
    structuredExcel(inputfile, converted_files, company_name, sep)


#if __name__=="__main__":
#    inputfile = sys.argv[1]#'F:\Data_files2\DELTON CABLES LTDAnnual_Report_16-17.pdf'
#    company_name = sys.argv[2]
#
#    if inputfile.count('\\'):
#        sep = '\\'
#    else:
#        sep = '/'
#
#    obj = GetFinPages()
#    obj.get_fin_pages(inputfile)
#
#    os.chdir(obj.f_path)
#    splited_files = obj.splited_page
#    #print(time.time()-start_time)
#    print('\n'.join(splited_files))
#    converted_files = convert(obj.splited_page)
#    structuredExcel(converted_files,company_name,sep)
    
    