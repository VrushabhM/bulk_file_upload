from flask import Flask, request
from flask_restplus import Resource, Api, fields, apidoc
from flask_restplus import reqparse
from flask_celery import make_celery
import ast
import main
from gevent.threadpool import ThreadPool
from pymongo import MongoClient
from bson import ObjectId
#from Status import GetStatusSingle, GetStatusALL
#import Status
client = MongoClient()
db = client.celery

pool = ThreadPool(10)

app = Flask(__name__)
app.config['CELERY_BROKER_URL'] = 'redis://localhost'
app.config['CELERY_RESULT_BACKEND'] = 'mongodb://localhost:27017/'
celery = make_celery(app)

api = Api(app, version='1.0', title='File Upload API',
          description='Extraction of Balance sheet and Profit & Loss from Annual Report')


@celery.task(bind=True, name="BulkUpload.read_file")
def read_file(self, item):
    inputfile = item['file_path']
    company_name = item['company_name']

    main.main(inputfile, company_name)
    task_id = read_file.request.id
    return task_id


def iterate_files(file_list):
    task_ids = []
    file_list = [ast.literal_eval(item) for item in file_list]
    while file_list:
        item = file_list.pop(0)
        _id = read_file.delay(item)
        dd = {"_id": str(_id), "file_path": item['file_path']}
        task_ids.append(dd)

    data = {'job_ids': task_ids}
    res = db.bulk_task.insert_one(data)
    main_id = res.inserted_id
    d = {"main_id": str(main_id), 'job_ids': task_ids}
    return d


class BulkFileUpload(Resource):
    parser1 = reqparse.RequestParser()
    parser1.add_argument("my_list", action='append', help='Bad choice: {error_msg}')

    @api.doc(parser=parser1)
    def post(self):
        """ process list of files for extraction"""
        args = self.parser1.parse_args()
        file_list = args['my_list']
        if len(file_list) <= 10:
            d = iterate_files(args['my_list'])
            return d
        else:
            return "Number of files exceeds limit of 10"


class FileUpload(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument("file_path", required=True, help="absolute file path")
    parser.add_argument("company_name", required=True, help="absolute file path")

    @api.doc(parser=parser)
    def post(self):
        """ Upload single file for extraction"""
        args = self.parser.parse_args()
        pay_load = {"file_path": args['file_path'], "company_name": args['company_name']}
        _id = read_file.delay(pay_load)
        d = {"_id": str(_id), "file_path": args['file_path']}
        return d


class GetStatusSingle(Resource):
    @api.doc()
    def get(self, _id):
        """ Get status of single process"""

        d2 = list(db.celery_taskmeta.find({"_id": _id}))
        if len(d2):
            status = d2[0]['status']
        else:
            status = 'PENDING'

        ret_val = {'status': status}

        return ret_val


class GetStatusALL(Resource):

    def get_status(self, item):
        d2 = list(db.celery_taskmeta.find({"_id": item['_id']}))
        if len(d2):
            status = d2[0]['status']
        else:
            status = 'PENDING'

        ret_val = {'status': status, "file_path": item['file_path']}

        return ret_val

    @api.doc()
    def get(self, main_id):
        """ Get status of all process"""
        m_id = ObjectId(main_id)
        d = list(db.bulk_task.find({"_id": m_id}))
        d = d[0]
        process_ids = d['job_ids']

        status_list = []
        for item in process_ids:
            status = self.get_status(item)
            status_list.append(status)

        res_list = {"main_id": main_id,
                    "status_list": status_list}

        return res_list


api.add_resource(BulkFileUpload, '/bulk_file_upload')
api.add_resource(FileUpload, '/file_upload')
api.add_resource(GetStatusALL, '/get_status_all/<main_id>')
api.add_resource(GetStatusSingle, '/get_status/<_id>')


if __name__ == "__main__":
    app.run(debug=True, threaded=True)
