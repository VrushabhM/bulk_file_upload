# -*- coding: utf-8 -*-
"""
Created on Tue Apr 17 15:14:02 2018

@author: DM390
"""

from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfdevice import PDFDevice, TagExtractor
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import TextConverter

from io import BytesIO


class ReadPdf(object):
    """ Reads pdf file """

    def __init__(self):
        pass


    def get_text(self,infile, page,LA_analysis=False):
        """ Reads pdf by page and return text in byte
            
            Parameters:
            infile (file obj): open(file_path) read object
            page (int): page number to read text from
            LA_analysis (boolean): To enable or disable Layout Analysis 
            
            Returns:
            text (byte): returns pdfs text in byte format     

        """

        output = BytesIO()
        #output = StringIO()
        manager = PDFResourceManager()
        if LA_analysis:
            converter = TextConverter(manager, output)#, laparams=LAParams())
        else:
            converter = TextConverter(manager, output)
            
        interpreter = PDFPageInterpreter(manager, converter)
        
        for page in PDFPage.get_pages(infile, set([page])):
            interpreter.process_page(page)

        converter.close()
        self.text = output.getvalue()
        output.close()
        return self.text 


if __name__=="__main__":
    file = 'F:\Data_files2\MCA_balance_sheet_31-03-2015.pdf'
    obj = ReadPdf
    infile = open(file,'rb')
    text = obj.get_text(infile,2)
    

