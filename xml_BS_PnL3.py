# -*- coding: utf-8 -*-
"""
Created on Wed Feb 14 12:24:25 2018

@author: DM390
"""

import xml.etree.ElementTree as ET
from collections import OrderedDict
import pandas
import os
from lxml import etree
import getIndex
import codecs
#xml_file = 'BS_Coffee-day-annual-report-2015_page_44.xml'


def to_xml(inputfile):
    outputfile = inputfile.split('.pdf')[0]+'.xml'
    cmd = 'pdftohtml -c -hidden -xml "'+inputfile+'" "'+outputfile+'"'
    os.popen(cmd)
    
    return outputfile


def get_group2(texts):
      date_index = getIndex.get_date_index(texts)
      cord_dict = OrderedDict()
      for text_tag in texts[:50]:
            top = int(text_tag.attrib['top'])
            left = int(text_tag.attrib['left'])
            cord_dict.setdefault(top, []).append(left)
      
      max_length = len(max(cord_dict.values(),key=len))
      
      temp_list = []
      for value in cord_dict.values():
            if len(value) == max_length:
                  temp_list.append(value)
      
      
      n = len(temp_list)
      l = [sum(i)/n for i in zip(*temp_list)]      
      #group_list = [approximate_num(i) for i in l] 
      group_list = [int(i) for i in l]
      
      group_list = list(set(group_list))
      group_list.sort()

      return group_list


def get_group(texts):
      
      left_list = []
      for i in texts[:20]:
            head = i.find('b')
            if isinstance(head,ET.Element):
                  left_list.append(int(i.attrib['left']))
            else:
                  continue
      
      left_list = list(set(left_list))
      left_list.sort()
      
      group_list = []
      for i in left_list:
            if not group_list:
                  group_list.append(i)
                  continue
            else:
                  if i - group_list[-1] < 90:
                        continue
                  else:
                        group_list.append(i)
      
      return group_list


def get_mapped_lines(texts):
      date_index = getIndex.get_date_index(texts)
      top_list = []
      for i in texts[date_index+1:]:
            top_list.append(int(i.attrib['top']))      

      mapped_dict = {}
      i = 0;k = 2    
      while k != len(top_list):
            first = top_list[i]
            third = top_list[k]
            
            if first == third:
                  middle = top_list[i+1]
                  
                  if middle != first:
                        if abs(middle-first) < 5:
                              mapped_dict[top_list[i+1]] = first
                        elif abs(middle-first) > 5:
                              mapped_dict[top_list[i+1]] = first+1
      
            i += 1;k += 1
                  
      return mapped_dict


def get_lines(texts):
      mapped_dict = get_mapped_lines(texts)

      lines = OrderedDict()
      for text_tag in texts:
            text = ' '.join([i for i in text_tag.itertext()])
            text = text.strip()
            if text:
                  top = int(text_tag.attrib['top'])
                  if top in mapped_dict.keys():
                        top = mapped_dict[top]

                  left = int(text_tag.attrib['left'])
                  lines.setdefault(top, []).append([left,text])
                  
      return lines


def get_value_cords(lines,group_list):
      temp_list = []
      for key,value in lines.items():
            for val in value:
                  l = []
                  for i in group_list:
                        l.append([abs(val[0]-i),key,i,val[1]])
                  temp_list.append(min(l)[1:])
      
      return temp_list


def get_table(lines,temp_list,group_list):
      index = list(lines.keys())
      index.sort()
      index2 = [i for i in range(len(index))]
      df = pandas.DataFrame(index = index2,columns=group_list)
      
      mapped_list = [k for k in zip(index,[i for i in range(len(index))])]
      mapped_index = {}
      for ind in mapped_list:
            mapped_index[ind[0]] = ind[1]

      for value in temp_list:
            ind = mapped_index[value[0]]
            col = value[1]
            cell_value = value[2]
            df.iloc[ind][col] = cell_value

      return df


def get_xml_data(xml_file):
      with open(xml_file,'rb') as fd:
           xml_string = fd.read()

      xml_parser = etree.XMLParser(recover=True)
      root = etree.fromstring(xml_string, parser=xml_parser)
      #root = ET.fromstring(xml_string,parser)
      texts = root.findall('page/text')
      #group_list = get_group(texts)
      group_list = get_group2(texts)
      lines = get_lines(texts)
      cell_cords = get_value_cords(lines,group_list)
      
      main_df = get_table(lines,cell_cords,group_list)
      out_file = xml_file.split(".xml")[0]+".xlsx"
      main_df.to_excel(out_file)
      return out_file,main_df
 
#xml_file = '/media/vrushabh/TECHNOLOGY/my_stuff/Updated/bs_pnlExtract/files/Bilt-Graphic-Paper-Annual-Report-2016-17_page_50.xml'     
#xml_file = '/media/vrushabh/TECHNOLOGY/my_stuff/data_files/Audited_Shilpi Cables_2015/splited_pages/BS_Audited_Shilpi Cables_2015_page_92.xml'
#xml_file = '/media/vrushabh/TECHNOLOGY/my_stuff/Updated/bs_pnlExtract/files/PnL_Shilpi Cable Annual Report 2017_page_75.xml'
#xml_file = '/media/vrushabh/TECHNOLOGY/my_stuff/data_files/Mangalore Chemicals 2017/splited_pages/BS_Mangalore Chemicals 2017_page_48.xml'
#continue
#xml_file = "BS_Shilpi Cable Annual Report 2017_page_74.xml"
#xml_file = "BS_SIL_Investments_page_55.xml"
#xml_file = "BS_Coffee-day-annual-report-2015_page_44.xml"
#xml_file = "BS_NCC Annual Report2014_15_page_62.xml"
#
#xml_file = "PnL_Coffee-day-annual-report-2015_page_45.xml"
#xml_file = "PnL_Mangalore Chemicals 2017_page_49.xml"
#xml_file = "PnL_NCC Annual Report2014_15_page_63.xml"
#xml_file = "PnL_Shilpi Cable Annual Report 2017_page_75.xml"
#
#with open(xml_file,'rb') as fd:
#     xml_string = fd.read()
#
#from lxml import etree
#parser = etree.XMLParser(recover=True)
#root = etree.fromstring(xml_string, parser=parser)
#
##root = ET.fromstring(xml_string,parser)
#
#text_list = []
#texts = root.findall('page/text')
##group_list = get_group(texts)
#group_list = get_group2(texts)
#lines = get_lines(texts)
#cell_cords = get_value_cords(lines,group_list)
#
#main_df = get_table(lines,cell_cords,group_list)
