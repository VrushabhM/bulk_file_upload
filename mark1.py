# -*- coding: utf-8 -*-
"""
Created on Mon Nov 27 09:44:46 2017

@author: DM390
"""

import pandas
import codecs
import re
from dateutil import parser
import sqlalchemy,pyodbc
import numpy as np

from datetime import datetime

engine = sqlalchemy.create_engine('mssql+pyodbc://sa:miswak@365@DBserver2016')

def get_date(df):
    date = ''
    date_pattern = r'(Jan(uary)?|Feb(ruary)?|Mar(ch)?|Apr(il)?|May|Jun(e)?|Jul(y)?|Aug(ust)?|Sep(tember)?|Oct(ober)?|Nov(ember)?|Dec(ember)?)(|\s+\d{1,2})(\s+\d{4})'
    #pattern = '(?<=As at).+'
    pattern = '\d{1,2} \d{1,2} \d{1,4}'
    dates = []
    
    for ind in range(6):
        for i in df.columns:
            s = ' '.join(re.findall('\w+',df.at[ind,i]))
            if re.search(pattern,s,re.IGNORECASE):
                l = re.findall(pattern,s,re.IGNORECASE)
                if l:
                    dd = parser.parse(l[0],dayfirst=True)
                    month = dd.strftime("%b").title()
                    year = dd.strftime("%y")
                    date = month+'.'+year
                    dates.append(date)

            elif re.search(date_pattern,s,re.IGNORECASE):
                l = re.findall(date_pattern,s,re.IGNORECASE)
                date = l[0][0][:3]+'.'+l[0][-1][-2:]
                dates.append(date)
    
    return dates


def split_cells(df,sep='\n'):

    df.fillna('',inplace=True)
    main_df = pandas.DataFrame()
    for ind in df.index:
        d = df.iloc[[ind]];d = d.applymap(lambda x:str(x))
        l = d.applymap(lambda x: x.split(sep))
        l = [list(row)[0] for index,row in l.iteritems()]
        max_length = len(max(l))

        l3 = [['' for j in range(max_length-len(i))]+i if len(i) < max_length else i for i in l ]    
        df3 = pandas.DataFrame(l3).T
        main_df = main_df.append(df3,ignore_index=True)

    return main_df


def get_index(df):
    last_index = 0
    df = df.applymap(lambda x:x.lower() if x else '')
    for col in df.columns:
        temp = df.loc[(df[col].str.contains('as per our')) |(df[col].str.lower()=='total') |(df[col].str.contains('significant accounting'))]
        if temp.size:
            last_index = temp.index[-1]            

    return last_index


def delete_col(df,value):
    for col in df.columns:
        temp_df = df[col];temp_df.dropna(inplace=True);temp_df = temp_df[:7]
        temp_df = temp_df.apply(lambda x:x.lower() if isinstance(x,str) else x)
        temp = temp_df.str.contains(value)
        temp = temp.dropna()
        temp = [i for i in temp if i]
        if temp:
            df.drop(col,axis=1,inplace=True)
            break
    
    return df


def format_data(df,labels):
#    lables = []
#    for col in df.columns:
#        temp_df = df[col];temp_df.dropna(inplace=True)
#        l2 = list(temp_df)
#        lables.extend([' '.join(re.findall(r'\w+',i,re.IGNORECASE))for i in l2 if isinstance(i,unicode)])
    
    cols = list(df.columns)
    main_df = pandas.DataFrame()
    
    for label in labels:
        #print lable
        temp_df = df.apply(lambda x:x.str.lower())
        temp_df.fillna("",inplace=True)
        for col in df.columns:
            ind = temp_df[col][temp_df[col].str.contains(label)].index.tolist()
            if ind:
                temp_cols = cols[cols.index(col):]
                d = df.iloc[[ind[0]]]
                d = d[temp_cols]            
                #l = pandas.DataFrame([i for i in d.values[0] if not pandas.isnull(i)]).T
                main_df = main_df.append(d,ignore_index=True)
                break

    for c in main_df:
        if sum(main_df[c].isnull()) >= 2:
            main_df.drop(c, axis=1, inplace=True)

    return main_df


def get_details(df,excel_path,last_index):

    temp_df = df[last_index+1:]#; temp_df = temp_df.applymap(str)
    #temp_df = temp_df.applymap(lambda x:str(x))
    temp = []
    for col in temp_df.columns:
        temp.extend(list(temp_df[col]))

    #temp = [i for i in temp] #str(i).decode('ascii',errors='ignore').encode('ascii')
    
    keywords = {
    'Chartered Accountants':['chartered accountants','chartered accounts'],
    'Director':['m.d.','managing director','executive director','director','directors','managing director & ceo','director - works'],
    'Partner':['partner'],
    'Company Secretary':['secretary','company secretary','company sec'],
    'Chief Financial Officer':['cfo','chief financial officer'],
    }

    main_list = []#;l = []
    for key,values in keywords.items():
        temp = [i.replace('(','').replace(')','') for i in temp]
        for index,value in enumerate(temp):
            if values.count(value.lower()):
                l = []#if re.match()
                #print(key,temp[index-1])
                l.extend([key,temp[index-1]])
                if re.search(r'\d{1,5}',temp[index+1]):
                    #print(temp[index+1])
                    l.append(temp[index+1])
                else:
                    continue
                #main_list.append(l)
        main_list.append(l)

    #out_path = excel_path.split('.xlsx')[0]+'_OtherDetails'+'.xlsx'
    df = pandas.DataFrame(main_list)
    #df.to_excel(out_path)
    return df


def get_value(df):   
    pattern = 'in (lacs|crore|million)'
    l = df.applymap(lambda x: x if re.search(pattern,x,re.IGNORECASE) else np.nan)
    
    l.dropna(how='all',inplace=True)
    l.dropna(axis=1,how='all',inplace=True)
    
    if len(l):
        amount = l.loc[l.index[0]].values[0]
        amount = ' '.join(re.findall(r'\w+',amount)).lower()

        if amount.count('million'):
            return 1000000
        elif amount.count('crore'):
            value = 10000000/100000.0
            return value
        elif amount.count('lacs'):
              value = 1
              return value
    else:
          print("Amount in Rupess")
          return 1/100000    # for rupess to lacs


def save_in_database(df,company_name,dates,mul):

   
    cols = list(df.columns)
    d = df[cols[0]].apply(lambda x: ' '.join(re.findall(r'\w+',str(x),re.IGNORECASE)).title())
    
    df1 = pandas.concat([d,df[cols[-2]]],axis=1)
    df2 = pandas.concat([d,df[cols[-1]]],axis=1) 
    df1.columns = [0,1];df2.columns = [0,1]

#    mon_yr = get_date(df1)
    df1['Year'] = dates.pop(0)
 #   mon_yr = get_date(df2)
    df2['Year'] = dates.pop(0)
    
    result = df2.append(df1)
    result = result[pandas.notnull(result[1])]
    result = result[result[0] != 'Nan']
    result = result.rename(columns={0:'Label',1:'Value'})
    result  = result.apply(pandas.to_numeric,errors='ignore')
    result['Value'] = result['Value'].apply(lambda x: x*mul if isinstance(x,float) else x)
    
    result['Quarter']     = '12 mths'
    result['ValuesIN']    = 'Rs Lacs'
    result['Name']        = company_name
    result['DateFetched'] = datetime.now()
    result.to_excel('result.xlsx')
    #result.to_sql(name='PDFBalanceSheet',con=engine,if_exists = 'append',index = False)#,dtype = TypeDict)    
    print("Saved in database")


def format_data2(df,labels):
#    lables = []
#    for col in df.columns:
#        temp_df = df[col];temp_df.dropna(inplace=True)
#        l2 = list(temp_df)
#        lables.extend([' '.join(re.findall(r'\w+',i,re.IGNORECASE))for i in l2 if isinstance(i,unicode)])
    
    cols = list(df.columns)
    main_df = pandas.DataFrame()
    
    for label in labels:
        #print lable
        temp_df = df.apply(lambda x:x.str.lower())
#        temp_df = df.apply(lambda x:' '.join(re.findall(r'\w+',x) 
#                           if isinstance(x,unicode) else x))

        temp_df.fillna("",inplace=True)
        for col in df.columns:
            ind = temp_df[col][temp_df[col].str.lower() == label.lower()].index.tolist()
            print(ind)
            if ind:
                temp_cols = cols[cols.index(col):]
                d = df.iloc[[ind[0]]]
                d = d[temp_cols]
                df.drop(ind[0],inplace=True)
                #l = pandas.DataFrame([i for i in d.values[0] if not pandas.isnull(i)]).T
                main_df = main_df.append(d,ignore_index=True)
                break

    for c in main_df:
        if sum(main_df[c].isnull()) >= 2:
            main_df.drop(c, axis=1, inplace=True)

    return main_df

#key_words = list(pandas.read_excel('mapping.xlsx')['Label'])
#labels = [x.lower() for x in key_words]

#temp_df = split_cells(df)
#dates = get_date(df)
#last_index = get_index(temp_df)
#other_df = get_details(temp_df,'',last_index)
#
#temp_df = temp_df[:last_index+1]
#mul = get_value(temp_df)
#temp_df = delete_col(temp_df,'note')
#main_df = format_data(temp_df,labels)
#
#save_in_database(main_df,'Coffee day',dates,mul)
#
#import numpy as np
#df3  = main_df.apply(pandas.to_numeric,errors='ignore')

#d = temp_df[temp_df['2'].str.contains(label).shift(1,axis=1).fillna(False)]
