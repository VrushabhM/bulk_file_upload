# -*- coding: utf-8 -*-
"""
Created on Tue Apr 17 16:20:35 2018

@author: DM390
"""


from datetime import datetime
import re,os,sys
from PyPDF2 import PdfFileWriter, PdfFileReader
import gevent
from gevent.threadpool import ThreadPool

from ReadPdf import ReadPdf
pdf = ReadPdf()

class SepFinPages(object):
    
    def __init(self):
        pass#self.pdf = ReadPdf()

    def count_match_kw(self,keywords,text):

        """count number of keyword mathched in text
           Params:
           keywords (list): keyowrds to match in text
           text (string) : pdf text
        """
        
        pattern = '('+'|'.join(keywords)+')'
        match = re.findall(pattern,text,re.IGNORECASE)
        l = list(set([i.lower() for i in match]))
        
        match_count = 0
        for i in keywords:
            if l.count(i):
                match_count += 1
    
        return match_count
    
    
    def is_balance_sheet(self,text):
        
        """ checks wether text contains balance sheet """

        BS_keywords = ['balance sheet', "shareholders  fund", 'non current liabilities',
                     'current liabilities', 'assets', 'non current assets', 'current assets']
    
        t = re.findall(r'\w+',text)
        text = ' '.join(t)                 
        match_count = self.count_match_kw(BS_keywords,text)
        if match_count == len(BS_keywords)-1:
            return True
    
    
    def is_PnL(self,text):
        """ checks wether text contains Profit and Loss """

        PnL_keywords = ['profit and loss','revenue','other income','total revenue',
                        'expenses','finance costs','before tax','tax expense']
    
        match_count = self.count_match_kw(PnL_keywords,text)                 
        if match_count >= len(PnL_keywords)-1:
            return True
        

    def make_dir(self,in_file,out_file):
        """ """
        dir_name = in_file.split('.pdf')[0]
        print(dir_name)
        if not os.path.isdir(dir_name):
            os.mkdir(in_file.split('.pdf')[0])
            splited_folder = os.path.join(dir_name,'splited_pages')
            os.mkdir(splited_folder)
            return splited_folder
        else:
            print('Directory already exists')
            sys.exit()
    
        
    #splited_file = file_name.split(sep)[-1].split('.pdf')[0]+'_'+p
    def save_pages(self,path,inputfile,pages_path,file_name,pages):
          inputpdf = PdfFileReader(open(inputfile, "rb"))
    
          #output = PdfFileWriter()
          temp = []
          for page in pages:
              output = PdfFileWriter()
              #out_file = path+'\\'+f_name.split('.pdf')[0]+'_page_'+str(page+1)+'.pdf'
              out_file = os.path.join(path,'page_'+str(page+1)+'.pdf')#os.path.join(path,f_name.split('.pdf')[0]+'_page_'+str(page+1)+'.pdf')
              output.addPage(inputpdf.getPage(page))
              with open(out_file, "wb") as outputStream:
                    output.write(outputStream)
              temp.append([out_file,file_name,pages_path])
    
          return temp



    def split_pages(self,page_l):
        #global pages_path,pages_found,file_name1,sep
        page = page_l[0]
        infile = open(page, 'rb')
        file_name = page_l[1]
        pages_path = page_l[2]
    
        #obj = SepFinPages
        
        #f_thred = FileObjectThread(infile,'rb')
        if page.count('\\'):
            sep = '\\'
        else:
            sep = '/'
        
        text = pdf.get_text(infile,0)
        #text = text.replace('&','and')
        text = text.decode('utf-8',errors='ignore')
        text = re.sub(r'[^\w]',' ',text)
    
        p = page.split(sep)[-1]
        #page_no = int(re.findall('\d+',p)[0])
        if self.is_balance_sheet(text):
            print("BS found in "+ str(page))
            #;page_no = int(re.findall('\d+',p)[0])
            #p = p.replace(str(page_no),str(page_no+1))
            splited_file = 'BS_'+file_name.split(sep)[-1].split('.pdf')[0]+'_'+p
       
            out_file = os.path.join(pages_path,splited_file)
            infile.close()
            os.rename(page,out_file)
            #pages_found.append(out_file)
    
        elif self.is_PnL(text):
            print("PnL found in "+ str(page))
            #p = page.split(sep)[-1]#;page_no = int(re.findall('\d+',p)[0])
            #p = p.replace(str(page_no),str(page_no+1))
            splited_file = 'PnL_'+file_name.split(sep)[-1].split('.pdf')[0]+'_'+p
            out_file = os.path.join(pages_path,splited_file)
            infile.close()
            os.rename(page,out_file)
            #pages_found.append(out_file)
        else:
              infile.close()
              os.remove(page)    
    
        
class  GetFinPages:
    
    """ Detects Pages with Financial Information
        Example: Balane sheet/Profit and Loss
        Type: Class
    """
    def __init__(self):
        self.f_path = ''
        self.pages_path = ''
        self.splited_page = ''
        
        
    def get_fin_pages(self,input_file,pool_size=10):

        """ Detects and saves financial pages in file_path/splited_pages
            
            Parameters:
            input_file (file obj): open(file_path) read object
            pool_size (int): Number of Thread pools
        """
        obj = SepFinPages()
        pool = ThreadPool(pool_size)
        
        print("Started for "+input_file)
    
        if input_file.count('\\'):
            file_name = input_file.split('\\')[-1]
            sep = '\\'
        else:
            file_name = input_file.split('/')[-1]
            sep='/'
    
        infile1 = open(input_file, 'rb')
        total_pages = PdfFileReader(infile1).numPages
        print("Total pages "+str(total_pages))
        infile1.close()
        self.f_path =sep.join(input_file.split(sep)[:-1])+sep
        self.pages_path = obj.make_dir(input_file,'')
        #pickle = 
        #pages_path = os.path.join(f_path,"pages")
        #os.mkdir(pages_path)
        
        remainder = total_pages%pool_size
        pages = total_pages-remainder
        p = 0
        for i in range(pool_size,pages+10,pool_size):
            page_list = obj.save_pages(self.pages_path,input_file,self.pages_path,file_name,range(p,i))
            p = i
            pool.map(obj.split_pages,page_list)
            pool.kill()
    #        time.sleep(1)
    #        for s_file in to_remove:
    #              os.remove(s_file)
            print(str(i)+" pages done out of "+str(total_pages))
            
        page_list = obj.save_pages(self.pages_path,input_file,self.pages_path,file_name,range(p,remainder+1))
        pool.map(obj.split_pages,page_list)
        pool.kill()
    #    time.sleep(1)
    #    for s_file in to_remove:
    #          os.remove(s_file)
    
    
        print(str(i+remainder)+" pages done out of "+str(total_pages))
        self.splited_page = [os.path.join(self.pages_path,page) for page in os.listdir(self.pages_path)]
        #return self.f_path,pages_path,self.splited_page
