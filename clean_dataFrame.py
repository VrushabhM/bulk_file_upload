# -*- coding: utf-8 -*-
"""
Created on Sun Feb 25 13:37:26 2018

@author: DM390
"""
import re
import pandas
from dateutil import parser
from datetime import datetime
import numpy as np

def format_dataframe(df,thresh = 3):
    temp_df = df.applymap(lambda x:str(x))
    temp_df = temp_df.applymap(lambda x:' '.join(re.findall(r'\w+',x)).strip() if not re.search(r'\d+',x) else x)
    temp_df = delete_col(temp_df,'note')
    
    temp_df.columns = [str(i) for i in range(len(temp_df.columns)) ]
    cols = list(temp_df.columns)
    
    temp = []
    for ind in temp_df.index:
        d = [[ind,col] for col in cols if len(temp_df.at[ind,col]) > thresh]
        if len(d):
            temp.append(d[0])    
    #temp_df = df.applymap(lambda x:str(x))
    #temp_df = delete_col(temp_df,'note')
    temp_df.columns = [str(i) for i in range(len(temp_df.columns)) ]
    cols = list(temp_df.columns)
    
    main_df = pandas.DataFrame()
    for item in temp:    
        temp_cols = cols[cols.index(item[1]):]
        d = temp_df.loc[item[0],temp_cols]#;d = d.apply(lambda x: x if x else np.nan)
        d.dropna(inplace=True);
        #d.index = range(len(d.index))
        main_df=main_df.append(d,ignore_index=True)
        
    return main_df


def remove_roman(df):
    roman_num = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII','VIII', 'IX', 'X',
                 'XI', 'XII', 'XIII', 'XIV', 'XV', 'XVI', 'XVII', 'XVIII', 'XIX', 'XX',
                 'XXI', 'XXII', 'XXIII', 'XXIV', 'XXV', 'XXVI', 'XXVII', 'XXVIII', 'XXIX', 'XXX']
    
    
    pattern = '|'.join([r'(?!:\w+)'+i+'+'+r'(?!\w+)' for i in roman_num])
    dd = df.applymap(lambda x: re.sub(pattern,' ',x))

#    pattern = ' '+r' | '.join(roman_num)+' '
#    pattern = r'|'.join(roman_num)
#    dd = dd.applymap(lambda x: re.sub(pattern,' ',x))
    return dd

    

def delete_col(df,value):
    for col in df.columns:
        temp_df = df[col];temp_df.dropna(inplace=True);temp_df = temp_df[:7]
        temp_df = temp_df.apply(lambda x:x.lower() if isinstance(x,str) else x)
        temp = temp_df.str.contains(value)
        temp = temp.dropna()
        temp = [i for i in temp if i]
        if temp:
            df.drop(col,axis=1,inplace=True)
            break
    
    return df


def format_dataframe2(temp_df):
      temp_df.fillna('',inplace=True)  
      #removes (a),(1) and (i) like strings
      df3 = temp_df.applymap(lambda x: re.sub(r"(\(\w{1}\)|\([ixv]{1,6}\))",'',x,re.IGNORECASE)
                                          if not re.search(r'\d+',x) else x)
      df3 = remove_roman(df3)
      df4 = df3.applymap(lambda x:str(x))
      
      #removes special characters from dataframe
      df4 = df4.applymap(lambda x:' '.join(re.findall(r'\w+',x)).strip() if not re.search(r'\d+',x) else x)
      df4 = delete_col(df4,'note')
      
      df4.columns = [str(i) for i in range(len(df4.columns)) ]
      cols = list(df4.columns)
          
      temp = []
      for ind in df4.index:
            d = [[ind,col] for col in cols if len(df4.at[ind,col])]# > thresh]
            if len(d):
                  temp.append(d[0])    
          #temp_df = df.applymap(lambda x:str(x))
          #temp_df = delete_col(temp_df,'note')
      df4.columns = [str(i) for i in range(len(df4.columns)) ]
      cols = list(df4.columns)
          
      fin_df = pandas.DataFrame()
      for item in temp:    
            temp_cols = cols[cols.index(item[1]):]
            d = df4.loc[item[0],temp_cols]#;d = d.apply(lambda x: x if x else np.nan)
            d.dropna(inplace=True);
            #d.index = range(len(d.index))
            fin_df=fin_df.append(d,ignore_index=True)
  
        
      return fin_df


def format_dataframe3(temp_df):
      temp_df.fillna('',inplace=True)  
      #removes (a),(1) and (i) like strings
      df3 = temp_df.applymap(lambda x:x.replace('&','and'))
      df3 = df3.applymap(lambda x: re.sub(r"(\((?:| )\w{1}(?:| )\)|\([ixv]{1,6}\))",'',x,re.IGNORECASE)
                                          if re.search(r'\w+',x) else x)
      
      df3[0] = df3[0].apply(lambda x: '' if x.isdigit() and len(x) == 1 else x)
      df3 = remove_roman(df3)
      
      df4 = df3.applymap(lambda x:str(x))
      del df3
      #removes special characters from dataframe
      df4 = df4.applymap(lambda x:' '.join(re.findall(r'\w+',x)).strip() if not re.search(r'\d+',x) else x)
      df4 = delete_col(df4,'note')
      
      df4.columns = [str(i) for i in range(len(df4.columns)) ]
      cols = list(df4.columns)

      df4_temp = df4.copy()
      d = df4_temp[df4_temp.columns[0]]
      col = df4_temp.columns[1]
      for ind in d.index:
          if not d[ind]:
              if df4_temp.at[ind,col] and not re.search(r'\d+',df4_temp.at[ind,col]):
                  d[ind] = df4_temp.at[ind,col]
                  df4_temp.at[ind,col] = ''
          
          #temp_df = df.applymap(lambda x:str(x))
          #temp_df = delete_col(temp_df,'note')
      df4_temp['0'] = df4_temp['0'].apply(lambda x:x.title())    
      #df4_temp.columns = [str(i) for i in range(len(df4_temp.columns)) ]
      #cols = list(df4_temp.columns)
          
#      fin_df = pandas.DataFrame()
#      for item in temp:    
#            temp_cols = cols[cols.index(item[1]):]
#            d = df4.loc[item[0],temp_cols]#;d = d.apply(lambda x: x if x else np.nan)
#            d.dropna(inplace=True);
#            #d.index = range(len(d.index))
#            fin_df=fin_df.append(d,ignore_index=True)
  
        
      return df4_temp

  
def to_datetime(dates):
    date_times = [parser.parse(i,dayfirst=True) for i in dates] 
    month_years = [date.strftime('%b')+'.'+date.strftime('%y') for date in date_times]
    month_years = sorted(set(month_years), key=lambda x: month_years.index(x))
    return month_years


def get_date(df):
    df = df.applymap(lambda x:str(x))

    #date_pattern = r'(Jan(uary)?|Feb(ruary)?|Mar(ch)?|Apr(il)?|May|Jun(e)?|Jul(y)?|Aug(ust)?|Sep(tember)?|Oct(ober)?|Nov(ember)?|Dec(ember)?)(|\s+\d{1,2})(\s+\d{4})'
    date_pattern = r'(Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)|(|\s+\d{1,2})(\s+\d{4})'    
    date_pattern2 = r'(\d{1,2}\s+)(Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)(\s+\d{4})'
    pattern = '\d{1,2} \d{1,2} \d{1,4}'
    dates = []
    for ind in df.index:
        for i in df.columns:
            s = ' '.join(re.findall('\w+',df.at[ind,i]))
            if re.search(pattern,s,re.IGNORECASE):
                l = re.findall(pattern,s,re.IGNORECASE)
                date = ' '.join(l)
                #date = ' '.join([' '.join(filter(None,i)) for i in l])
                dates.append(date)

            elif re.search(date_pattern2,s,re.IGNORECASE):
                l = re.findall(date_pattern2,s,re.IGNORECASE)
                date = ' '.join([' '.join(filter(None,i)) for i in l])
                dates.append(date)

            elif re.search(date_pattern,s,re.IGNORECASE):
                l = re.findall(date_pattern,s,re.IGNORECASE)
                date = ' '.join([' '.join(filter(None,i)) for i in l])
                dates.append(date)

    return dates


def get_index(label_list,index='first'):

    label_list = list(label_list)

    if index == 'first':
        pattern = r'equity and liabilities|assets|revenue|revenue from operations'
        for ind,value in enumerate(label_list):
            if re.search(pattern,value,re.IGNORECASE):
                return ind
        
    elif index=='last':        
        pattern = r'as per our|significant accounting'
        for ind,value in enumerate(label_list):
            if re.search(pattern,value,re.IGNORECASE):
                return ind
            
    else:
        print('invalid index selection (check spelling of - first/last)')
                

def get_value(df):   
    #pattern = 'in (lacs|lakhs|crore|million)'
    pattern = 'in (crore|million|lacs|lakhs|rupees)'
    df = df.applymap(lambda x:str(x))
    l = df.applymap(lambda x: x if re.search(pattern,x,re.IGNORECASE) else np.nan)
    
    l.dropna(how='all',inplace=True)
    l.dropna(axis=1,how='all',inplace=True)
    
    if len(l):
        amount = l.loc[l.index[0]].values[0]
        amount = ' '.join(re.findall(r'\w+',amount)).lower()
        
        if amount.count('million'):
            return 'In Million'
            #return 1000000
        elif amount.count('crore'):
            return 'In Crore'
            #value = 10000000/100000.0
            #return value
        elif amount.count('lac') or amount.count('lakhs'):
            return 'In Lacs'
        
        elif amount.count('rupees'):
            return 'In Rupees'

    else:
        return 'In Rupees'


def convert_values(df):
    values = df['Value'].copy()
    values.fillna('',inplace=True)
      
    for ind in values.index:
          value = values.loc[ind]
          value = value.replace(',','')
          if value:
                if re.search(r'\(.+\)',value):
                      value = value.replace('(','').replace(')','')
                      try:
                            value = -float(value)
                      except:
                            pass
                      values.loc[ind] = value
                else:
                      try:
                            values.loc[ind] = float(value)
                      except:
                            pass
    
    df['Value'] = values
    return df


def get_table(temp_df):
    fin_df = format_dataframe3(temp_df)
    label_list = fin_df['0']
    first_index = get_index(label_list=label_list)
    last_index = get_index(label_list=label_list,index='last')
    
    # get dates
    dates = get_date(temp_df[0:first_index])
    month_years = to_datetime(dates)
    
    # get amount in
    amount_in = get_value(fin_df[0:first_index])
    
    df = fin_df[first_index:last_index]
    df = df.applymap(lambda x: x if x else np.nan)
    th = len(df)-len(df)%10
    df = df.dropna(how='all',thresh=len(df)-th,axis=1)
    df.columns = ['Label']+month_years
    df['AmountIN'] = amount_in
    #del df['AmountIn'] #= amount_in
    # to database format
    #Labels = df['Label']
    #df.to_excel('test.xlsx')
    return df

	
def to_databaseFormat(temp_df,company_name,writter,sheetName):
    #def to_databaseFormat(temp_df,company_name,result_file):    
    # main_df is from xml_BS_PnL extract
    #temp_df = main_df.copy()
    #df = temp_df[first_index:last_index]
    fin_df = format_dataframe3(temp_df)
    label_list = fin_df['0']
    first_index = get_index(label_list=label_list)
    last_index = get_index(label_list=label_list,index='last')
    
    
    # get dates
    dates = get_date(fin_df[0:first_index])
    month_years = to_datetime(dates)
    
    # get amount in
    amount_in = get_value(fin_df[0:first_index])
    
    
    df = fin_df[first_index:last_index]
    df = df.applymap(lambda x: x if x else np.nan)
    th = len(df)-len(df)%10
    df = df.dropna(how='all',thresh=len(df)-th,axis=1)
    df.columns = ['Label']+month_years
    
    #del df['AmountIn'] #= amount_in
    
    # to database format
    Labels = df['Label']
    df.to_excel('test.xlsx')
    return df
#    cols = list(df.columns[1:])
#    result = pandas.DataFrame()
#    for col in cols:
#        df1 = pandas.concat([Labels,df[col]],axis=1)
#        df1.rename(columns={col:'Value'},inplace=True)
#        df1['Year'] = col
#        result = result.append(df1,ignore_index=1)
#    
#    result = convert_values(result)
#    result['Quarter']     = '12mths'
#    result['AmountIN'] = amount_in
#    result['CompanyName'] = company_name
#    result['DateFetched'] = datetime.now()
#    #result.to_excel(writter,sheet_name=sheetName)
#    #result.to_excel(writter)
#    #writter.close()
#    #result.to_excel(result_file)
#    print("Saved in database")
#    return result
    
#result = to_databaseFormat(main_df,'zyz','','')
#""" old method of to database format """
#cols = list(df.columns)
#d = df[cols[0]].apply(lambda x: ' '.join(re.findall(r'\w+',str(x),re.IGNORECASE)).title())
#    
#result = pandas.DataFrame()
#for col in cols[1:]:
#      df1 = pandas.concat([d,df[col]],axis=1)
#      df1.columns = range(len(df1.columns))
#      df1['Year'] = month_years.pop(0)
#      result = result.append(df1,ignore_index=1)
#    
##    result = result[pandas.notnull(result[1])]
##    result = result[result[0] != 'Nan']
#result = result.rename(columns={0:'Label',1:'Value'})
#values = result['Value'].copy()
#values.fillna('',inplace=True)
#
#
#for ind in values.index:
#      value = values.loc[ind]
#      value = value.replace(',','')
#      if value:
#            if re.search(r'\(.+\)',value):
#                  value = value.replace('(','').replace(')','')
#                  try:
#                        value = -float(value)
#                  except:
#                        pass
#                  values.loc[ind] = value
#            else:
#                  try:
#                        values.loc[ind] = float(value)
#                  except:
#                        pass
#
#result['Value'] = values
#
#result['Value'] = result['Value'].apply(lambda x: x*mul if isinstance(x,float) else x)
#result['Quarter']     = '12mths'
#result['ValuesIN']    = 'Rs Lacs'
#result['Name']        = company_name
#result['DateFetched'] = datetime.now()
# 
#""" end """